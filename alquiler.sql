-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-07-2019 a las 01:16:12
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `alquiler`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `car`
--

CREATE TABLE `car` (
  `id_car` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `license_palte` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `car`
--

INSERT INTO `car` (`id_car`, `name`, `model`, `mileage`, `license_palte`, `created_at`, `updated_at`) VALUES
(1, 'nada', 'toyota', 444, '44llll', '2019-05-31 06:00:00', '2019-07-16 02:33:55'),
(2, 'nada', 'lllll', 444, '4444sss', '2019-05-31 11:04:32', '2019-05-31 11:04:32'),
(4, 'flota', 'nissan', 82, '44444ssss', '2019-07-16 02:35:43', '2019-07-16 02:35:43'),
(5, 'nada5', 'nada', 155, '1444', '2019-07-16 05:01:34', '2019-07-16 05:01:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_client` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci` int(11) DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_client`, `name`, `lastname`, `ci`, `mail`, `created_at`, `updated_at`) VALUES
(10, 'aaa', 'vargas', 123, 'w@l', '2019-07-14 06:00:00', '2019-07-14 06:00:00'),
(11, 'nada5', 'nada', 123, 'w@l', '2019-07-14 06:00:00', '2019-07-14 06:00:00'),
(12, 'juancito', 'carpas', 88888, 'wawrnig@live.com', '2019-07-15 03:47:03', '2019-07-15 03:47:03'),
(13, 'nada5', 'nada', 12355, 'a@l', '2019-07-14 06:00:00', '2019-07-14 06:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_05_31_021632_create_client_table', 1),
(2, '2019_05_31_021807_create_order_table', 1),
(3, '2019_05_31_022028_create_order_car_table', 1),
(4, '2019_05_31_022155_create_car_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `exit_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_client` text COLLATE utf8mb4_unicode_ci,
  `id_car` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order`
--

INSERT INTO `order` (`id`, `exit_date`, `delivery_date`, `total`, `state`, `id_client`, `id_car`, `created_at`, `updated_at`) VALUES
(4, '15 agosto', '20 agosto', NULL, 'bueno', '1111', '', '2019-05-31 10:48:28', '2019-05-31 10:48:28'),
(5, '15 agosto', '20 agosto', NULL, 'bueno', '1111', '', '2019-05-31 10:49:56', '2019-05-31 10:49:56'),
(6, '15 agosto', '20 agosto', NULL, 'bueno', '1111', '', '2019-05-31 10:51:00', '2019-05-31 10:51:00'),
(7, '15', '20', NULL, 'bueno', 'jose', 'camioneta', '2019-05-31 06:00:00', '2019-05-31 06:00:00'),
(8, '15/12/14', '20/14/05', 50, 'pesimo', 'david', 'trufi', '2019-07-16 02:20:22', '2019-07-16 02:20:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_car`
--

CREATE TABLE `order_car` (
  `id_order_car` bigint(20) UNSIGNED NOT NULL,
  `id_car` int(11) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_car`
--

INSERT INTO `order_car` (`id_order_car`, `id_car`, `id_order`, `created_at`, `updated_at`) VALUES
(3, 1, 2, '2019-05-31 09:11:27', '2019-05-31 09:11:27'),
(4, 5, 6, '2019-05-31 10:59:57', '2019-05-31 10:59:57'),
(5, 5, 6, '2019-05-31 11:01:38', '2019-05-31 11:01:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id_car`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_car`
--
ALTER TABLE `order_car`
  ADD PRIMARY KEY (`id_order_car`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `car`
--
ALTER TABLE `car`
  MODIFY `id_car` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id_client` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `order_car`
--
ALTER TABLE `order_car`
  MODIFY `id_order_car` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
